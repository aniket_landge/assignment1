//  ViewController.swift
//  Assignment1
//  Created by Aniket Landge on 20/01/22.

import UIKit

class LoginViewController: UITableViewController {
    //Login Screen
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passworField: UITextField!
    @IBOutlet var fbBtn: UIButton!
    @IBOutlet var googleBtn: UIButton!
    @IBOutlet var forgetBtn: UIButton!
    @IBOutlet var loginBtn: UIButton!
    
    //Calling Validation of email and password
    @IBAction func loginActionBtn(_ sender: UIButton) {
        validationText()
        emailField.text = ""
        passworField.text = ""
    }
    
    @IBAction func fbActionButton(_ sender: UIButton) {
        openAlert(title: "Facebook", message: "Login Successfully through facebook!", alertStyle: .alert, actionTitles: ["okay"], actionStyles: [.default], actions: [{
            _ in print("")
        }])
    }
    
    @IBAction func googleActionButton(_ sender: UIButton) {
        openAlert(title: "Google", message: "Login Successfully through Google!", alertStyle: .alert, actionTitles: ["okay"], actionStyles: [.default], actions: [{
            _ in print("")
        }])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Calling addBottomLineToTextField to both email and password field
        addBottomLineToTextField(textField: emailField)
        addBottomLineToTextField(textField: passworField)
        
        //Rounded Facebook button and style border
        fbBtn.backgroundColor = .clear
        fbBtn.layer.cornerRadius = 20
        fbBtn.layer.borderWidth = 1
        fbBtn.layer.borderColor = UIColor.systemBlue.cgColor
        
        //Rounded Google button and style border
        googleBtn.backgroundColor = .clear
        googleBtn.layer.cornerRadius = 20
        googleBtn.layer.borderWidth = 1
        googleBtn.layer.borderColor = UIColor.brown.cgColor
        
        //Rounded Login Button button
        loginBtn.layer.cornerRadius = 20
        
    }
    
}

extension String{
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    var isPasswordValid: Bool {
        let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
}

extension LoginViewController{
    // Global Alert function
    public func openAlert(title: String,message: String,alertStyle:UIAlertController.Style,actionTitles:[String],actionStyles:[UIAlertAction.Style],actions: [((UIAlertAction) -> Void)]){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        for(index, indexTitle) in actionTitles.enumerated(){
            let action = UIAlertAction(title: indexTitle, style: actionStyles[index], handler: actions[index])
            alertController.addAction(action)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    //Validation of email and password fields
    fileprivate func validationText(){
        if let email = emailField.text, let password = passworField.text{
            if email == "" && password == "" {
                openAlert(title: "Alert", message: "Please Enter credentials !", alertStyle: .alert, actionTitles: ["Okay"], actionStyles: [.default], actions: [{
                    _ in print("Okay Clicked.")
                }])
            }else{
                if !email.isValidEmail{
                    openAlert(title: "Email Alert", message: "Enter Valid Email !", alertStyle: .alert, actionTitles: ["Okay"], actionStyles: [.default], actions: [{
                        _ in print("Okay Clicked.")
                    }])
                }else if !password.isPasswordValid{
                    openAlert(title: "Password Alert", message: "Enter Valid Password !", alertStyle: .alert, actionTitles: ["Okay"], actionStyles: [.default], actions: [{
                        _ in print("Okay Clicked.")
                    }])
                }else{
                    let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                    self.navigationController?.pushViewController(detailViewController, animated: true)
                }
            }
        }
    }
    
    //Creating Bottom line for text fields
    func addBottomLineToTextField(textField : UITextField) {
        //1.Create Bottom Line with CALayer
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: textField.frame.height, width:textField.frame.width-45, height: 2)
        bottomLine.backgroundColor = UIColor.init(red: 0.3569, green: 0.3569, blue: 0.3569, alpha: 1.0).cgColor
        //2.Remove Border from text field
        textField.borderStyle = .none
        //3.Add line to new text field
        textField.layer.addSublayer(bottomLine)
    }
    
}


