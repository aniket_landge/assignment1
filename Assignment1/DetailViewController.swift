//  DetailViewController.swift
//  Assignment1
//  Created by Aniket Landge on 24/01/22.

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate{
    
    @IBOutlet var oneView: UITableView!
    @IBOutlet var tabbar: UITabBar!
    
    var name = ["Aniket", "Malhar", "Rahul", "Suyash", "Divyansh", "Neeraj", "Deepesh", "Devraj", "Sakshi", "Apeksha"]
    //    var img: [UIImage] = [
    //        UIImage(named: "Aniket.png")!,
    //        UIImage(named: "Malhar.png")!,
    //        UIImage(named: "Rahul.png")!,
    //        UIImage(named: "Suyash.png")!,
    //        UIImage(named: "Divyansh.png")!,
    //        UIImage(named: "Neeraj.png")!,
    //        UIImage(named: "Deepesh.png")!,
    //        UIImage(named: "Devraj.png")!,
    //        UIImage(named: "Sakshi.png")!,
    //        UIImage(named: "Apeksha.png")!,
    //    ]
    
    //Return count of number of rows should be there in table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    //Cell content
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Reusable cells
        //":tableViewCell" means in which each item should return
        let cell: tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! tableViewCell
        let person = name[indexPath.row]
        cell.nameLbl.text = person
        cell.img.image = UIImage(named: person)
        //      cell.img.image = img[indexPath.row]
        return cell
    }
    
    //Height For row should  return
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Saved"
        navigationController?.navigationBar.prefersLargeTitles = true
        oneView.allowsSelection = false
        configureItems()
        tabbar.delegate = self
    }
    
    //Navigation Items 
    func configureItems(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: nil
        )
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .edit,
            target: self,
            action: nil
        )
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "Voicemail"{
            let viewController = storyboard?.instantiateViewController(withIdentifier: "VoiceMailViewController") as! VoiceMailViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}

class VoiceMailViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Voicemail"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}
