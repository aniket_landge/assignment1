//  tableViewCell.swift
//  Assignment1
//
//  Created by Aniket Landge on 24/01/22.


import UIKit

//Cell class
class tableViewCell: UITableViewCell {
    
    @IBOutlet var oneView: UIView!
    @IBOutlet var img: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
}
